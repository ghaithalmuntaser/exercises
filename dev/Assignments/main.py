from mads_datasets import DatasetFactoryProvider, DatasetType

penguins = DatasetFactoryProvider.create_factory(DatasetType.PENGUINS)
penguins.download_data()
penguins.filepath
import pandas as pd

df = pd.read_parquet(penguins.filepath)
old_lenght= len(df)
df.head()
(df.isna().sum() > 0).count()

# REPLACE NAN SEX VALUE WITH UNKNOWN
def FillNanSex(df: pd.DataFrame, column: str, old_value: str, new_value: str):
    df[column].fillna(new_value, inplace=True)
    return df
df = FillNanSex(df, "Sex", "NaN", "Unknown")

# fill the nan age value with the mean
def fillInBodyMass (df , column):
    mean_mass= df[column].mean()
    df[column].fillna(mean_mass, inplace=True)
    return df
fillInBodyMass (df , "Body Mass (g)")
df.head()


#Drop nan rows of comment 
def DropOfEmptyComment (df, columns):
    empty_commnts = df[df[columns].isna()].index.tolist()
    df.drop(empty_commnts, inplace=True)
    return df
DropOfEmptyComment (df , "Comments")
new_length= len(df)
#SAVE THE NEW FILE

from datetime import datetime
# Define the path where you want to save the CSV file
output_path = "/Users/ghaith/Desktop/excesise_4/data/final"
tag = datetime.now().strftime("%Y%m%d-%H%M") + ".csv"
output = output_path + "/" + tag  # Full path including the filename

# Assuming you have a DataFrame named 'df'
#df.to_csv(output, index=False)  # Write the DataFrame to the specified CSV file

# You can access the filepath using the 'output' variable
print(output,"\nold lenght =", old_lenght, "\nNew lenght = ",new_length)