import matplotlib.pyplot as plt  # noqa: INP001
import pandas as pd
import seaborn as sns
import streamlit as st
from mads_datasets import DatasetFactoryProvider, DatasetType


def load_penguins_dataset() -> pd.DataFrame:
    """_summary_
    allow_output_mutation=True argument is used because the
    penguinsdataset object is mutable, and we want to allow modifications to it.

    In addition to that, we want to cache the object, so that it is not reloaded
    every time the user interacts with the dashboard.
    """
    penguinsdataset = DatasetFactoryProvider.create_factory(DatasetType.PENGUINS)
    penguinsdataset.download_data()
    df = pd.read_parquet(penguinsdataset.filepath)  # noqa: PD901
    select = [
        "Species",
        "Island",
        "Culmen Length (mm)",
        "Culmen Depth (mm)",
        "Flipper Length (mm)",
        "Delta 15 N (o/oo)",
        "Delta 13 C (o/oo)",
        "Sex",
        "Body Mass (g)",
    ]

    return df[select].dropna()

def load_iris_dataset() -> pd.DataFrame:
    """_summary_
    allow_output_mutation=True argument is used because the
    penguinsdataset object is mutable, and we want to allow modifications to it.

    In addition to that, we want to cache the object, so that it is not reloaded
    every time the user interacts with the dashboard.
    """
    irisdataset = DatasetFactoryProvider.create_factory(DatasetType.IRIS)
    irisdataset.download_data()
    df = pd.read_csv(
        irisdataset.filepath,
        header=None,
        names=["sepal_length", "sepal_width", "petal_length", "petal_width", "class"],
    )
    select1 = [
        "sepal_length",
        "sepal_width",
        "petal_length",
        "petal_width",
        "class",
    ]

    return df[select1].dropna()

def main() -> None:
    if "penguins" not in st.session_state:
        st.session_state.penguins = load_penguins_dataset()

    if "IRIS" not in st.session_state:
        st.session_state.IRIS = load_iris_dataset()

    Available_dataset= ['IRIS', 'penguins']
    available_plot_types = ['scatterplot', 'lineplot', 'barplot']   
    option4 = st.selectbox(
        "Select DataSet",
        Available_dataset
    )

    option1 = st.selectbox(
        "Select the x-axis",
        
        st.session_state.IRIS.columns if option4 == "IRIS" else st.session_state.penguins.columns,
        index=2,
    )
    option2 = st.selectbox(
        "Select the y-axis",
        st.session_state.IRIS.columns if option4 == "IRIS" else st.session_state.penguins.columns,
        index=3,
    )
    option3 = st.selectbox(
        "Select plot type",
        available_plot_types
    )

    color = st.selectbox(
        "Select the color",
        st.session_state.IRIS.columns if option4 == "IRIS" else st.session_state.penguins.columns,
        index=0
    )

    fig, ax = plt.subplots()
   
# Get the selected plot type dynamically using getattr
    plot_function = getattr(sns, option3)

# Create the selected plot
    plot_function(data=st.session_state.IRIS if option4 == "IRIS" else st.session_state.penguins, x=option1, y=option2, hue=color)

    st.pyplot(fig)

if __name__ == "__main__":
    main()
