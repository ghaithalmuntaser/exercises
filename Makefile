
.PHONY: install test lint format

install:
	pdm install

test:
	pdm run excersise_5.py

lint:
	pdm run ruff excesise_4
	pdm run mypy excesise_4

format:
	pdm run isort -v excesise_4
	pdm run black excesise_4
