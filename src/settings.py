from pathlib import Path

from loguru import logger
from pydantic import BaseModel, validator


class Settings(BaseModel):
    basedir: Path
    datadir: Path
    outputdir: Path
    logdir: Path

    namecol: str

    @validator("*")
    def check_and_create_directories(cls, v):  # noqa: N805, ANN001, ANN201
        if isinstance(v, Path) and not v.exists():
            v.mkdir(parents=True)
            logger.info(f"Created directory: {v}")
        return v
